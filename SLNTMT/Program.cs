﻿using System;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SLNTMT
{

    class CheckoutData
    {
        //predefined info for checkout
        String FirstNameStr = "Alexander";
        String LastNameStr = "Bayer";
        String StreetAddress = "Longroad str.";
        String AddressLine2 = "";
        String CityNameStr = "Stafford";
        String CountryName = "United States";
        String StateProvinceStr = "Connecticut";
        String PostZipStr = "15532";
        String PhoneStr = "176299231";
        String EmailStr = "tddest@yger.net";

        public string FirstNameStr1 { get => FirstNameStr; set => FirstNameStr = value; }
        public string LastNameStr1 { get => LastNameStr; set => LastNameStr = value; }
        public string StreetAddress1 { get => StreetAddress; set => StreetAddress = value; }
        public string AddressLine21 { get => AddressLine2; set => AddressLine2 = value; }
        public string CityNameStr1 { get => CityNameStr; set => CityNameStr = value; }
        public string CountryName1 { get => CountryName; set => CountryName = value; }
        public string StateProvinceStr1 { get => StateProvinceStr; set => StateProvinceStr = value; }
        public string PostZipStr1 { get => PostZipStr; set => PostZipStr = value; }
        public string PhoneStr1 { get => PhoneStr; set => PhoneStr = value; }
        public string EmailStr1 { get => EmailStr; set => EmailStr = value; }
    }

    class WebActionClass
    {

        CheckoutData dataForCheckout;

        String baseShopUrl = "https://www.supremesclothingonline.com/";
        //String sampleElement = "https://www.supremesclothingonline.com/supreme-20fw-icy-arc-hooded-sweatshirt-3-colors-p-6244.html?zenid=sbqs1fhf3bn693qoslbqkk8nu5";

        //we are creating 2 drivers to handle the operation
        //driver for adding to cart
        IWebDriver currentDriver;
        //driver for checkout
        IWebDriver cartDriver;

        ChromeOptions chromeOptionsCurrent;
        ChromeOptions chromeOptionsCart;

        Boolean SetUpChromeDriver()
        {
            //Setting up the drivers
            chromeOptionsCurrent = new ChromeOptions();
            chromeOptionsCurrent.AddUserProfilePreference("profile.default_content_setting_values.images", 2);
            chromeOptionsCurrent.AddArgument("headless");
            chromeOptionsCurrent.AddArgument("--blink-settings=imagesEnabled=false");

            currentDriver = new ChromeDriver("/home/cpyrg/Documents/chromedriver", chromeOptionsCurrent);


            chromeOptionsCart = new ChromeOptions();
            chromeOptionsCart.AddUserProfilePreference("profile.default_content_setting_values.images", 2);
            chromeOptionsCart.AddArgument("headless");
            chromeOptionsCart.AddArgument("--blink-settings=imagesEnabled=false");

            cartDriver = new ChromeDriver("/home/cpyrg/Documents/chromedriver", chromeOptionsCart);


            if (currentDriver != null || cartDriver != null)
            {
                //currentDriver.Manage().Cookies.DeleteAllCookies();
                //cartDriver.Manage().Cookies.DeleteAllCookies();

                return true;
            }
            else
            {
                return false;
            }
        }

        Boolean SetUpCart()
        {
            NavigateToBaseShop();
            return true;
        }

        Boolean FindAndClickCheckoutButton()
        {
            IWebElement checkoutButtonElement = currentDriver.FindElement(By.Id("CHECKOUT"));
            if (checkoutButtonElement == null)
            {
                return false;
            }
            else
            {
                //trying to hit the button
                checkoutButtonElement.Submit();
                return true;
            }
        }


        Boolean NavigateToBaseShop()
        {
            cartDriver.Navigate().GoToUrl(baseShopUrl);
            return true;
        }

        Boolean NavigateToElement(String elementUrl)
        {
            currentDriver.Navigate().GoToUrl(elementUrl);
            return true;
        }

        Boolean AddElementFromUrlToCart(String elementUrl)
        {
            NavigateToElement(elementUrl);

            String addToCartButtonCSS = ".buybtn";
            //adding to the cart
            Console.WriteLine("Adding to cart");
            IWebElement checkoutButtonElement =
                currentDriver.FindElement(By.CssSelector(addToCartButtonCSS));

            checkoutButtonElement.Submit();

            //Thread.Sleep(500);

            return true;
        }

        Boolean FillTheFormForCheckout()
        {

            //first we finding all elments
            //then clicking on thems

            String privacyCheckButtonCSS = "#privacy";
            IWebElement privacyCheckButtonElement =
                currentDriver.FindElement(By.CssSelector(privacyCheckButtonCSS));

            String firstNameFieldCSS = "#firstname";
            IWebElement firstNameFieldElement =
                currentDriver.FindElement(By.CssSelector(firstNameFieldCSS));

            String lastNameFieldCSS = "#lastname";
            IWebElement lastNameFieldElement =
                currentDriver.FindElement(By.CssSelector(lastNameFieldCSS));

            String streetFieldCSS = "#street-address";
            IWebElement streetFieldElement =
                currentDriver.FindElement(By.CssSelector(streetFieldCSS));

            String billingAddressFieldCSS = ".fec-billing-address > .fec-field:nth-child(7)";
            IWebElement billingAddressFieldElement =
                currentDriver.FindElement(By.CssSelector(billingAddressFieldCSS));

            String suburbFieldCSS = "#suburb";
            IWebElement suburbFieldElement =
                currentDriver.FindElement(By.CssSelector(suburbFieldCSS));

            String cityFieldCSS = "#city";
            IWebElement cityFieldElement =
                currentDriver.FindElement(By.CssSelector(cityFieldCSS));

            String countryFieldCSS = "#country";
            IWebElement countryFieldElement =
                currentDriver.FindElement(By.CssSelector(countryFieldCSS));

            String countryNameFieldCSS = "#country > option:nth-child(2)";
            IWebElement countryNameFieldElement =
                currentDriver.FindElement(By.CssSelector(countryNameFieldCSS));

            String stateZoneFieldCSS = "#stateZone";
            IWebElement stateZoneFieldElement =
                currentDriver.FindElement(By.CssSelector(stateZoneFieldCSS));

            String stateZoneNameFieldCSS = "#stateZone > option:nth-child(15)";
            IWebElement stateZoneNameFieldElement =
                currentDriver.FindElement(By.CssSelector(stateZoneNameFieldCSS));

            String postCodeFieldCSS = "#postcode";
            IWebElement postCodeFieldElement =
                currentDriver.FindElement(By.CssSelector(postCodeFieldCSS));

            String emailFieldCSS = "#email-address";
            IWebElement emailFieldElement =
                currentDriver.FindElement(By.CssSelector(emailFieldCSS));

            String phoneFieldCSS = "#telephone";
            IWebElement phoneFieldElement =
                currentDriver.FindElement(By.CssSelector(phoneFieldCSS));

            //starting to click and enetering data
            //clicking the privacy button
            privacyCheckButtonElement.Click();

            //Clicking and typing the name
            firstNameFieldElement.Click();
            firstNameFieldElement.SendKeys("Alexander");

            //last name
            lastNameFieldElement.Click();
            lastNameFieldElement.SendKeys(dataForCheckout.LastNameStr1);


            streetFieldElement.Click();
            streetFieldElement.SendKeys(dataForCheckout.StreetAddress1);

            billingAddressFieldElement.Click();

            suburbFieldElement.Click();

            cityFieldElement.Click();
            cityFieldElement.SendKeys(dataForCheckout.CityNameStr1);

            countryFieldElement.Click();

            countryNameFieldElement.Click();

            stateZoneFieldElement.Click();

            stateZoneNameFieldElement.Click();

            postCodeFieldElement.Click();
            postCodeFieldElement.SendKeys(dataForCheckout.PostZipStr1);

            emailFieldElement.Click();
            emailFieldElement.SendKeys(dataForCheckout.EmailStr1);

            phoneFieldElement.Click();
            phoneFieldElement.SendKeys(dataForCheckout.PhoneStr1);

            return true;
        }

        Boolean BuyElementsInCart()
        {
            foreach (Cookie cookie in currentDriver.Manage().Cookies.AllCookies)
            {
                cartDriver.Manage().Cookies.AddCookie(cookie);
            }

            String openCartButtonCSS = ".cartqty";
            IWebElement openCartButtonElement =
                cartDriver.FindElement(By.CssSelector(openCartButtonCSS));
            openCartButtonElement.Click();

            FillTheFormForCheckout();

            return true;
        }


        public void PerformAction()
        {
            if (SetUpChromeDriver() == false)
            {
                return;
            }

            //setting up the cart
            SetUpCart();

            dataForCheckout = new CheckoutData();

            Stopwatch stopWatchGeneral = new Stopwatch();
            Console.WriteLine("Stopwatch started");
            stopWatchGeneral.Start();

            String elementsListFile = "ListOfItems.txt";

            if (File.Exists(elementsListFile) != true)
            {
                Console.WriteLine("File does not exist");
                return;
            }

            //reading all items from the input text file
            string[] itemElementsUrls = System.IO.File.ReadAllLines(elementsListFile);
            Console.WriteLine("Listing all elements in the input text file");
            foreach (string currentLine in itemElementsUrls)
            {
                Console.WriteLine("------------------------------------");
                Console.WriteLine("Adding the following element to cart");
                Console.WriteLine(currentLine);
                Stopwatch stopWatchAddToCart = new Stopwatch();
                stopWatchAddToCart.Start();
                AddElementFromUrlToCart(currentLine);
                stopWatchAddToCart.Stop();
                Console.WriteLine("Elapsed time for adding the element: " + stopWatchGeneral.ElapsedMilliseconds.ToString() + " msec");
            }

            //BuyElementsInCart();

            //checkout
            Stopwatch stopWatchCheckout = new Stopwatch();
            stopWatchCheckout.Start();

            Console.WriteLine("Checkout");
            String checkoutFromCartButtonCSS = ".forward img";

            IWebElement checkoutFromCartButtonElement =
                currentDriver.FindElement(By.CssSelector(checkoutFromCartButtonCSS));
            checkoutFromCartButtonElement.Click();

            //guest checkout
            String guestCheckoutButtonCss = ".buttonRow img";
            IWebElement guestCheckoutButtonElement =
                currentDriver.FindElement(By.CssSelector(guestCheckoutButtonCss));
            guestCheckoutButtonElement.Click();

            //Filling the form
            Console.WriteLine("Filling the form");
            FillTheFormForCheckout();

            //continueCheckoutButton
            Console.WriteLine("Continuing");

            stopWatchCheckout.Stop();
            Console.WriteLine("Elapsed time for checkout: " + stopWatchCheckout.ElapsedMilliseconds.ToString() + " msec");

            stopWatchGeneral.Stop();
            Console.WriteLine("Elapsed time total: " + stopWatchGeneral.ElapsedMilliseconds.ToString() + " msec");

            /*String continueCheckoutButtonCss = "#checkoutButton > input";
            IWebElement continueCheckoutButtonElement =
                currentDriver.FindElement(By.CssSelector(continueCheckoutButtonCss));
            continueCheckoutButtonElement.Click();

            //clicking the free shipping button
            String freeShippingButtonCss = "#ship-freeoptions-freeoptions";
            IWebElement freeShippingButtonElement =
                currentDriver.FindElement(By.CssSelector(freeShippingButtonCss));
            freeShippingButtonElement.Click();

            //clicking the cont
            continueCheckoutButtonCss = ".forward > input";
            continueCheckoutButtonElement =
                currentDriver.FindElement(By.CssSelector(continueCheckoutButtonCss));
            continueCheckoutButtonElement.Click();*/
        }

        public WebActionClass()
        {

        }

    }


    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Selenium driver test");

            WebActionClass actClass = new WebActionClass();

            actClass.PerformAction();

        }
    }
}
